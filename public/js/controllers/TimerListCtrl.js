(function() {
	'use strict';

	angular.module('intervalTimer')
		.controller('TimerListCtrl', ['$scope', '$uibModalInstance', 'timersStorage',
			function($scope, $uibModalInstance, timersStorage) {
				$scope.timers = timersStorage.timers;

				$scope.load = function (timer) {
					$uibModalInstance.close(timer);
				};

				$scope.delete = function (timer) {
					timersStorage.delete(timer);
				};

				$scope.cancel = function () {
					$uibModalInstance.dismiss('cancel');
				};
		}]);
})();