(function() {
	'use strict';

	angular.module('intervalTimer')
		.controller('TimerSaveCtrl', ['$scope', '$uibModalInstance', function($scope, $uibModalInstance) {
			$scope.name = '';

			$scope.ok = function () {
				$uibModalInstance.close($scope.name);
			};

			$scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};
		}]);
})();