(function() {
	'use strict';

	angular.module('intervalTimer')
		.controller('IntervalCtrl', ['$scope', '$uibModalInstance', 'interval', function($scope, $uibModalInstance, interval) {
			$scope.interval = {
				hours: parseInt(interval.time / 3600) % 24,
				minutes: parseInt(interval.time / 60) % 60,
				seconds: interval.time % 60,
				name: interval.name
			};

			$scope.ok = function () {
				$scope.interval.hours = parseInt($scope.interval.hours) || 0;
				$scope.interval.minutes = parseInt($scope.interval.minutes) || 0;
				$scope.interval.seconds = parseInt($scope.interval.seconds) || 0;
				$uibModalInstance.close({
					time: $scope.interval.hours * 3600 + $scope.interval.minutes * 60 + $scope.interval.seconds,
					name: $scope.interval.name
				});
			};

			$scope.cancel = function () {
				$uibModalInstance.dismiss('cancel');
			};
		}]);
})();