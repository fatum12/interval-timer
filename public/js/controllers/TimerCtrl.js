(function() {
	'use strict';

	angular.module('intervalTimer')
		.controller('TimerCtrl', ['$scope', '$interval', '$uibModal', '$notification', 'title', 'timeFilter', 'sound', 'timersStorage',
			function($scope, $interval, $uibModal, $notification, title, timeFilter, sound, timersStorage) {
				var timer = this,
					timerPromise;

				timer.STATUS_STOP = 0;
				timer.STATUS_PAUSE = 1;
				timer.STATUS_PLAY = 2;

				timer.intervals = [
					{
						time: 12,
						name: 'Example interval'
					}
				];
				timer.timers = timersStorage.timers;
				timer.status = timer.STATUS_STOP;
				timer.currentInterval = null;
				timer.loop = true;
				timer.showNotifications = true;
				timer.playSound = false;
				timer.currentIntervalIndex = 0;
				timer.sortableOptions = {
					handle: ' .interval-handle',
					forceHelperSize: true,
					forcePlaceholderSize: true
				};

				$scope.$watch('timer.playSound', function(playSound) {
					if (playSound) {
						sound.unmute();
					} else {
						sound.mute();
					}
				});

				timer.showAddForm = function() {
					var modalInstance = $uibModal.open({
						templateUrl: 'addIntervalModal.html',
						controller: 'IntervalCtrl',
						resolve: {
							interval: function () {
								return {
									time: 0,
									name: ''
								};
							}
						}
					});

					modalInstance.result.then(function (interval) {
						timer.intervals.push(interval);
					});
				};

				timer.removeInterval = function(interval) {
					var index = timer.intervals.indexOf(interval);
					if (index > -1) {
						timer.intervals.splice(index, 1);
					}
				};

				timer.editInterval = function(interval) {
					var index = timer.intervals.indexOf(interval);
					if (index > -1) {
						var modalInstance = $uibModal.open({
							templateUrl: 'addIntervalModal.html',
							controller: 'IntervalCtrl',
							resolve: {
								interval: function () {
									return interval;
								}
							}
						});

						modalInstance.result.then(function (interval) {
							timer.intervals[index] = interval;
						});
					}
				};

				timer.totalTime = function() {
					var total = 0;
					angular.forEach(timer.intervals, function(interval) {
						total += interval.time;
					});

					return total;
				};

				timer.isStatus = function(status) {
					return timer.status == status;
				};

				timer.setStatus = function(status) {
					timer.status = status;
				};

				timer.start = function() {
					if (!timer.intervals.length) {
						return;
					}
					timer.setStatus(timer.STATUS_PLAY);

					if (timer.currentInterval === null) {
						// first run
						timer.currentIntervalIndex = 0;
						timer.currentInterval = copyInterval(0);
						updateTitle();
						sound.play('short');
					}

					timerPromise = $interval(function() {
						timer.currentInterval.pastTime++;
						updateTitle();
						if (timer.currentInterval.pastTime >= timer.currentInterval.time) {
							// end of the current interval
							timer.currentIntervalIndex++;
							if (timer.currentIntervalIndex >= timer.intervals.length) {
								// last interval in set
								if (timer.loop) {
									timer.currentIntervalIndex = 0;
								} else {
									sound.play('longEnd');
									timer.stop();
									return;
								}
							}
							else {
								sound.play('long');
							}
							timer.currentInterval = copyInterval(timer.currentIntervalIndex);
							updateTitle();
							if (timer.currentIntervalIndex == 0) {
								sound.play('short');
							}
							if (timer.showNotifications) {
								$notification(timer.currentInterval.name ? timer.currentInterval.name : 'Interval', {
									body: timeFilter(timer.currentInterval.time)
								});
							}
						}
					}, 1000);
				};

				timer.stop = function() {
					timer.setStatus(timer.STATUS_STOP);
					$interval.cancel(timerPromise);
					timer.currentInterval = null;
					title.reset();
				};

				timer.pause = function() {
					timer.setStatus(timer.STATUS_PAUSE);
					$interval.cancel(timerPromise);
				};

				timer.saveState = function() {
					$uibModal
						.open({
							templateUrl: 'timerSaveModal.html',
							controller: 'TimerSaveCtrl'
						})
						.result.then(function (timerName) {
							timersStorage.add({
								name: timerName,
								date: Date.now(),
								intervals: timer.intervals,
								options: {
									loop: timer.loop,
									showNotifications: timer.showNotifications,
									playSound: timer.playSound
								}
							});
						});
				};

				timer.loadState = function() {
					$uibModal
						.open({
							templateUrl: 'timerListModal.html',
							controller: 'TimerListCtrl'
						})
						.result.then(function (state) {
							timer.intervals = state.intervals;
							var optionName;
							for (optionName in state.options) {
								timer[optionName] = state.options[optionName];
							}
						});
				};

				function copyInterval(index) {
					var copy = angular.copy(timer.intervals[index]);
					copy.pastTime = 0;
					return copy;
				}

				function updateTitle() {
					title.set(timeFilter(timer.currentInterval.pastTime) + ' / ' + timeFilter(timer.currentInterval.time) +
						(timer.currentInterval.name ? ' ' + timer.currentInterval.name : ''));
				}
		}]);
})();