(function() {
	'use strict';

	angular.module('intervalTimer')
		.filter('time', function() {
			return function(input) {
				input = input || 0;
				var hours = parseInt(input / 3600) % 24,
					minutes = parseInt(input / 60) % 60,
					seconds = input % 60,
					result = (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);

				if (hours > 0) {
					result = (hours < 10 ? '0' + hours : hours) + ":" + result;
				}

				return result;
			}
		});
})();