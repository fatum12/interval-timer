(function() {
	'use strict';

	angular.module('intervalTimer')
		.factory('title', ['$window', function($window) {
			var baseTitle = '';
			return {
				set: function(title) {
					if (baseTitle !== '' && title != baseTitle) {
						title = title + ' | ' + baseTitle;
					}
					$window.document.title = title;
				},
				setBase: function(title) {
					baseTitle = title;
				},
				reset: function() {
					this.set(baseTitle);
				}
			};
		}]);
})();