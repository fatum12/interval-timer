(function() {
	'use strict';

	angular.module('intervalTimer')
		.factory('sound', function() {
			var sounds = [],
				mute = false;

			return {
				init: function(soundsList) {
					var soundId,
						source;

					for (soundId in soundsList) {
						source = soundsList[soundId];
						if (!angular.isArray(source)) {
							source = [source];
						}
						sounds[soundId] = new Howl({
							src: source,
							autoplay: false,
							loop: false
						});
					}
				},
				play: function(soundId) {
					if (soundId in sounds && !mute) {
						sounds[soundId].play();
					}
				},
				mute: function() {
					mute = true;
				},
				unmute: function() {
					mute = false;
				}
			};
		});
})();