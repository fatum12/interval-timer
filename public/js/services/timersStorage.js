(function() {
	'use strict';

	angular.module('intervalTimer')
		.factory('timersStorage', ['localStorageService', function(localStorageService) {
			return {
				timers: localStorageService.get('timers') || [],
				add: function (timer) {
					this.timers.push(timer);
					this.save();
				},
				delete: function(timer) {
					var index = this.timers.indexOf(timer);
					if (index > -1) {
						this.timers.splice(index, 1);
						this.save();
					}
				},
				save: function() {
					localStorageService.set('timers', this.timers);
				}
			};
		}]);
})();