(function() {
	'use strict';

	angular.module('intervalTimer', ['ngTouch', 'ui.bootstrap', 'ui.sortable', 'notification', 'LocalStorageModule'])
		.config(['$notificationProvider', 'localStorageServiceProvider', function($notificationProvider, localStorageServiceProvider) {
			$notificationProvider.setOptions({
				tag: 'interval-timer',
				delay: 5000
			});

			localStorageServiceProvider.setPrefix('oddIntervalTimer');
		}])
		.run(['$notification', 'title', 'sound', function($notification, title, sound) {
			$notification.requestPermission();
			title.setBase('Interval Timer');
			sound.init({
				short: 'sounds/a6-sine-100ms.mp3',
				long: 'sounds/a6-sine-750ms.mp3',
				longEnd: 'sounds/a6-sine-750ms-end.mp3'
			});
		}]);
})();